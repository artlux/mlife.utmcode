<?php
namespace Mlife\Utmcode;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Events {
	
	//�������� �����
	public static function OnBeforeEventSend($arFields, $eventMessage){
		if((defined("CHK_EVENT") && CHK_EVENT===true)) return;
		global $APPLICATION;
		
		$utmData = array('utm_source','utm_medium','utm_term','utm_campaign','utm_content');
		
		$arFields['MLIFE_UTMCODE'] = '';
		$arUtm = array();
		if($_SESSION['mlife_utm_exists']){
			foreach($utmData as $v){
				if($_SESSION['mlife_'.$v]) $arUtm['mlife_'.$v] = $_SESSION['mlife_'.$v];
			}
		}elseif($APPLICATION->get_cookie('mlife_utm_exists')){
			foreach($utmData as $v){
				if($APPLICATION->get_cookie('mlife_'.$v)) $arUtm['mlife_'.$v] = $APPLICATION->get_cookie('mlife_'.$v);
			}
		}
		
		if(!empty($arUtm)){
			foreach($arUtm as $code=>$value){
				$value = urldecode($value);
				if(toUpper(SITE_CHARSET) !== 'UTF-8') $value = $APPLICATION->ConvertCharset($value, 'UTF-8', SITE_CHARSET);
				$arFields['MLIFE_UTMCODE'] .= Loc::getMessage('MLIFE_UTMCODE_'.toUpper(substr($code,6))).': '.$value."\n";
				$arFields['MLIFE_UTMCODE_'.toUpper(substr($code,6))] = $value;
			}
		}
		
	}
	
	
	public static function OnBeforeAdd(\Bitrix\Main\Entity\Event $event){
		if((defined("CHK_EVENT") && CHK_EVENT===true)) return;
		global $APPLICATION;
		
		$result = new \Bitrix\Main\Entity\EventResult;
		$data = $event->getParameter("fields");
		
		//echo'<pre>';print_r($data);echo'</pre>';
		$arFields = $data["C_FIELDS"];
		
		
		
		$utmData = array('utm_source','utm_medium','utm_term','utm_campaign','utm_content');
		
		$arFields['MLIFE_UTMCODE'] = '';
		$arUtm = array();
		if($_SESSION['mlife_utm_exists']){
			foreach($utmData as $v){
				if($_SESSION['mlife_'.$v]) $arUtm['mlife_'.$v] = $_SESSION['mlife_'.$v];
			}
		}elseif($APPLICATION->get_cookie('mlife_utm_exists')){
			foreach($utmData as $v){
				if($APPLICATION->get_cookie('mlife_'.$v)) $arUtm['mlife_'.$v] = $APPLICATION->get_cookie('mlife_'.$v);
			}
		}
		
		if(!empty($arUtm)){
			foreach($arUtm as $code=>$value){
				$value = urldecode($value);
				if(toUpper(SITE_CHARSET) !== 'UTF-8') $value = $APPLICATION->ConvertCharset($value, 'UTF-8', SITE_CHARSET);
				$arFields['MLIFE_UTMCODE'] .= Loc::getMessage('MLIFE_UTMCODE_'.toUpper(substr($code,6))).': '.$value."\n";
				$arFields['MLIFE_UTMCODE_'.toUpper(substr($code,6))] = $value;
			}
		}
		
		//print_r($arFields);
		//$data
		$result->modifyFields(array('C_FIELDS' => $arFields));

		return $result;
		
	}
	
	//�������� ����� ���������� ������ �����
	public static function OnBeforeEventAdd($ev, $lid, $arFields, $eventMessage){
		if((defined("CHK_EVENT") && CHK_EVENT===true)) return;
		global $APPLICATION;
		
		$utmData = array('utm_source','utm_medium','utm_term','utm_campaign','utm_content');
		
		$arFields['MLIFE_UTMCODE'] = '';
		$arUtm = array();
		if($_SESSION['mlife_utm_exists']){
			foreach($utmData as $v){
				if($_SESSION['mlife_'.$v]) $arUtm['mlife_'.$v] = $_SESSION['mlife_'.$v];
			}
		}elseif($APPLICATION->get_cookie('mlife_utm_exists')){
			foreach($utmData as $v){
				if($APPLICATION->get_cookie('mlife_'.$v)) $arUtm['mlife_'.$v] = $APPLICATION->get_cookie('mlife_'.$v);
			}
		}
		
		if(!empty($arUtm)){
			foreach($arUtm as $code=>$value){
				$value = urldecode($value);
				if(toUpper(SITE_CHARSET) !== 'UTF-8') $value = $APPLICATION->ConvertCharset($value, 'UTF-8', SITE_CHARSET);
				$arFields['MLIFE_UTMCODE'] .= Loc::getMessage('MLIFE_UTMCODE_'.toUpper(substr($code,6))).': '.$value."\n";
				$arFields['MLIFE_UTMCODE_'.toUpper(substr($code,6))] = $value;
			}
		}
		
	}
	
	public static function OnProlog(){
		if((defined("CHK_EVENT") && CHK_EVENT===true)) return;
		global $APPLICATION;
		
		$utmData = array('utm_source','utm_medium','utm_term','utm_campaign','utm_content');
		
		$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
		$exists = false;
		
		foreach($utmData as $v){
			if($request->getQuery($v)){
				$APPLICATION->set_cookie('mlife_'.$v,htmlspecialcharsEx($request->getQuery($v)));
				$_SESSION['mlife_'.$v] = htmlspecialcharsEx($request->getQuery($v));
				$exists = true;
			}
		}
		
		if($exists) {
			$APPLICATION->set_cookie('mlife_utm_exists',1);
			$_SESSION['mlife_utm_exists'] = 1;
		}
	}
	
	
}