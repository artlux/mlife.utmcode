<?
IncludeModuleLangFile(__FILE__);

class mlife_utmcode extends CModule
{
	var $MODULE_ID = "mlife.utmcode";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function mlife_utmcode()
	{
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = GetMessage("MLIFE_UTM_CODE_COMPANY_NAME");
		$this->PARTNER_URI = "http://mlife-media.by/";
		$this->MODULE_NAME = GetMessage("MLIFE_UTM_CODE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MLIFE_UTM_CODE_DESCRIPTION");
		return true;
	}
	
	function InstallDB($arParams = array())
	{
		return true;
	}
	
	function UnInstallDB($arParams = array())
	{
		return true;
	}
	
	function InstallEvents()
	{
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->registerEventHandlerCompatible('main', 'OnBeforeEventSend', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnBeforeEventSend');
		$eventManager->registerEventHandlerCompatible('main', 'OnProlog', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnProlog');
		$eventManager->registerEventHandlerCompatible('main', 'OnBeforeEventAdd', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnBeforeEventAdd');
		$eventManager->registerEventHandler('main','\Bitrix\Main\Mail\Internal\Event::OnBeforeAdd',$this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnBeforeAdd');
		return true;
	}
	
	function UnInstallEvents()
	{
		UnRegisterModuleDependences("main", 'OnBeforeEventSend', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnBeforeEventSend');
		UnRegisterModuleDependences("main", 'OnProlog', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnProlog');
		UnRegisterModuleDependences("main", 'OnBeforeEventAdd', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnBeforeEventAdd');
		UnRegisterModuleDependences("main", '\Bitrix\Main\Mail\Internal\Event::OnBeforeAdd', $this->MODULE_ID, '\Mlife\Utmcode\Events', 'OnBeforeAdd');
		return true;
	}
	
	function InstallFiles($arParams = array())
	{
		return true;
	}
	
	function UnInstallFiles()
	{
		return true;
	}
	
	function DoInstall()
	{
		$this->InstallFiles();
		RegisterModule($this->MODULE_ID);
		$this->InstallEvents();
	}
	
	function DoUninstall()
	{
		$this->UnInstallFiles();
		$this->UnInstallEvents();
		UnRegisterModule($this->MODULE_ID);
	}

}
?>