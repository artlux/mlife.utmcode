<?
if(IsModuleInstalled("mlife.utmcode")) {
	$eventManager = \Bitrix\Main\EventManager::getInstance();
	$eventManager->registerEventHandlerCompatible('main', 'OnBeforeEventAdd', 'mlife.utmcode', '\Mlife\Utmcode\Events', 'OnBeforeEventAdd');
	$eventManager->registerEventHandler('main','\Bitrix\Main\Mail\Internal\Event::OnBeforeAdd','mlife.utmcode', '\Mlife\Utmcode\Events', 'OnBeforeAdd');
}
?>