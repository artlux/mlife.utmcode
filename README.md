**Расшифровка UTM меток в почтовые шаблоны.** 

модуль для CMS Битрикс

Автоматически добавляет макросы в почтовые шаблоны

Автоматически добавляет данные в сессию и куки
```
#!php
//#MLIFE_UTMCODE# - форматированный вывод всех меток;
//#MLIFE_UTMCODE_UTM_SOURCE# - Источник компании;
//#MLIFE_UTMCODE_UTM_MEDIUM# - Тип трафика;
//#MLIFE_UTMCODE_UTM_TERM# - Название компании;
//#MLIFE_UTMCODE_UTM_CAMPAIGN# - Идентификатор объявления;
//#MLIFE_UTMCODE_UTM_CONTENT# - Ключевое слово;

//$_SESSION['mlife_utm_exists']
//$_SESSION['mlife_utm_source']
//$_SESSION['mlife_utm_medium']
//$_SESSION['mlife_utm_term']
//$_SESSION['mlife_utm_campaign']
//$_SESSION['mlife_utm_content']

global $APPLICATION;
echo $APPLICATION->get_cookie('mlife_utm_exists');
echo $APPLICATION->get_cookie('mlife_utm_source');
echo $APPLICATION->get_cookie('mlife_utm_medium');
echo urldecode($APPLICATION->get_cookie('mlife_utm_term'));
echo urldecode($APPLICATION->get_cookie('mlife_utm_campaign'));
echo urldecode($APPLICATION->get_cookie('mlife_utm_content'));

```